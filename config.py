import commentjson, time, datetime, os

settings_file = "settings.json"
last_load_time = False
settings = {}

def load_settings():
    global settings, last_load_time
    try:
        with open(settings_file) as f:
            settings = commentjson.load(f)
            last_load_time = datetime.datetime.now()
    except:
        # try again
        time.sleep(0.1)
        load_settings()

def refresh_settings():
    modified_time = datetime.datetime.fromtimestamp(os.path.getmtime(settings_file))
    if last_load_time < modified_time:
        load_settings()
        return True
    else:
        return False

load_settings()
